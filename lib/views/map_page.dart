import 'package:AcessAbilities/app/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';

class SimpleMapView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
          title: new Text("Map",
              style: TextStyle(
                  fontSize: Constants.headerFontSize,
                  fontFamily: "Avenir",
                  fontWeight: FontWeight.w900,
                  color: Constants.black))),
      body: FullScreenWrapper(
          backgroundDecoration:
              new BoxDecoration(color: Constants.mapBackground),
          imageProvider: const AssetImage("images/aae_map.png"),
          initialScale: PhotoViewComputedScale.contained,
          minScale: PhotoViewComputedScale.contained),
      backgroundColor: Colors.white,
    );
  }

  void onScaleStateChanged(PhotoViewScaleState scaleState) {}
}

class FullScreenWrapper extends StatelessWidget {
  const FullScreenWrapper(
      {this.imageProvider,
      this.loadingChild,
      this.backgroundDecoration,
      this.minScale,
      this.maxScale,
      this.initialScale,
      this.basePosition = Alignment.center});

  final ImageProvider imageProvider;
  final Widget loadingChild;
  final Decoration backgroundDecoration;
  final dynamic minScale;
  final dynamic maxScale;
  final dynamic initialScale;
  final Alignment basePosition;

  @override
  Widget build(BuildContext context) {
    return Container(
        color: Colors.white,
        constraints: BoxConstraints.expand(
          height: MediaQuery.of(context).size.height,
        ),
        child: PhotoView(
          imageProvider: imageProvider,
          loadingChild: loadingChild,
          backgroundDecoration: backgroundDecoration,
          minScale: minScale,
          maxScale: maxScale,
          initialScale: initialScale,
          basePosition: basePosition,
        ));
  }
}
