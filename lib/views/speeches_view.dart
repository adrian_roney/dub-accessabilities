import 'package:flutter/material.dart';
import 'package:AcessAbilities/app/utils/constants.dart';

import 'list_view.dart';

class SpeechesView extends StatelessWidget {
  final Color color;

  SpeechesView(this.color);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 3,
        child: Scaffold(
            backgroundColor: Colors.white,
            appBar:
            TabBar(

              tabs: <Widget>[
                Column(children: <Widget>[
                  Spacer(flex: 10),
                  Text('Day 1', textAlign: TextAlign.center),
                  Spacer(flex: 2),
                  Text('5th Nov', textAlign: TextAlign.center),
                  Spacer(flex: 10),
                ]),
                Column(children: <Widget>[
                  Spacer(flex: 10),
                  Text(
                    'Day 2',
                    textAlign: TextAlign.center,
                  ),
                  Spacer(flex: 2),
                  Text('6th Nov', textAlign: TextAlign.center),
                  Spacer(flex: 10),
                ]),
                Column(children: <Widget>[
                  Spacer(flex: 10),
                  Text('Day 3', textAlign: TextAlign.center),
                  Spacer(flex: 2),
                  Text('7th Nov', textAlign: TextAlign.center),
                  Spacer(flex: 10),
                ]),
              ],
              unselectedLabelColor: Colors.grey,
              labelColor: Constants.aaeOrange,
            ),
            body: TabBarView(
              children: <Widget>[
                AAEListView(color: this.color),
                AAEListView(color: this.color),
                AAEListView(color: this.color)
              ],
            )));
  }
}
