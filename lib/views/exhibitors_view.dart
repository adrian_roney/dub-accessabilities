import 'dart:convert';

import 'package:AcessAbilities/app/datamodel/exhibitor.dart';
import 'package:AcessAbilities/app/utils/constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ExhibitorsView extends StatelessWidget {
  const ExhibitorsView({Key key, this.color}) : super(key: key);

  final Color color;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: new AppBar(
            title: new Text("Exhibitors",
                style: TextStyle(
                    fontSize: Constants.headerFontSize,
                    fontFamily: "Avenir",
                    fontWeight: FontWeight.w900,
                    color: Constants.black))),
        backgroundColor: this.color, //Color.fromARGB(255, 245, 245, 245),
        body: Container(
          // Use future builder and DefaultAssetBundle to load the local JSON file
          child: FutureBuilder(
              future: DefaultAssetBundle.of(context)
                  .loadString('json/exhibitors.json'),
              builder: (context, snapshot) {
                // Decode the JSON

                List<Exhibitor> exhibitors = parseJson(snapshot.data);

                return new ListView.builder(
                    itemCount: exhibitors == null ? 0 : exhibitors.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Column(children: <Widget>[
                        ListTile(
                          leading: getImage(exhibitors[index]),
                          title: Text(exhibitors[index].name,
                              style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.normal,
                                  color: Constants.grey)),
                          subtitle: Text("Stand No : " + exhibitors[index].stand,
                              style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.normal,
                                  color: Constants.grey)),
                          onTap: null,
                        ),
                        Container(
                          height: 1,
                          color: Constants.listSeperatoColor,
                        ),
                      ]);
                    });
              }),
        ));
  }

  List<Exhibitor> parseJson(String response) {
    if (response == null) {
      return [];
    }

    final parsed = json.decode(response).cast<Map<String, dynamic>>();
    return parsed
        .map<Exhibitor>((json) => new Exhibitor.fromJson(json))
        .toList();
  }

  Image getImage(Exhibitor exhibitor) {
    double imageDimensions = 100;

    if (exhibitor == null || exhibitor.imagefile == "") {
      return Image.asset("images/broken.png",
          width: imageDimensions, height: imageDimensions);
    } else {
      return Image.asset(
        exhibitor.imagefile,
        width: imageDimensions,
        height: imageDimensions,
      );
    }
  }
}
