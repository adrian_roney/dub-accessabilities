import 'package:AcessAbilities/views/venue_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:AcessAbilities/app/utils/constants.dart';
import 'package:AcessAbilities/views/speeches_view.dart';
import 'discover_page.dart';
import 'exhibitors_view.dart';
import 'map_page.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _currentIndex = 0;
  final List<Widget> _children = [
    DiscoverPageListView(),
    ExhibitorsView(),
    VenuePage(),
    SimpleMapView()
  ];

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
        body: _children[_currentIndex], // new

        bottomNavigationBar: BottomNavigationBar(
            backgroundColor: Constants.aaeOrange,
            onTap: onTabTapped, // new
            currentIndex: _currentIndex, // new
            items: [
              BottomNavigationBarItem(
                icon: new Icon(Icons.explore, color: Constants.aaeOrange),
                title: new Text('Discover',
                    style: TextStyle(color: Constants.aaeOrange)),
              ),
              BottomNavigationBarItem(
                  icon: Icon(Icons.contacts, color: Constants.aaeOrange),
                  title: Text('Exhibitors',
                      style: TextStyle(color: Constants.aaeOrange))),
              BottomNavigationBarItem(
                  icon: Icon(Icons.store, color: Constants.aaeOrange),
                  title: Text('Venue',
                      style: TextStyle(color: Constants.aaeOrange))),
              BottomNavigationBarItem(
                  icon: Icon(Icons.map, color: Constants.aaeOrange),
                  title:
                      Text('Event Map', style: TextStyle(color: Constants.aaeOrange)))
            ]));
  }

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }
}
