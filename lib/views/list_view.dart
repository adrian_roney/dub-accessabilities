import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:AcessAbilities/app/utils/constants.dart';


class AAEListView extends StatelessWidget {

  const AAEListView({Key key, this.color}) : super(key: key);

  final Color color;

  @override
  Widget build(BuildContext context) {
    const List<int> shades = <int>[50, 100, 200, 300, 400, 500, 600, 700, 800, 900];

    return Scaffold(

      backgroundColor: this.color, //Color.fromARGB(255, 245, 245, 245),
      body: SizedBox.expand(
        
        child: ListView.builder(
          padding: EdgeInsets.all(15.0),
          itemCount: shades.length,
          itemBuilder: (BuildContext context, int index) {
            return SizedBox(
              child: Card(
                color: Constants.white,
                child: Padding(
                  padding: EdgeInsets.all(10.0),
                  child:InkWell(
                    onTap: () {
                      Navigator.pushNamed(context, "/text");
                    },
                    child: Center(
                      child: Text('Item $index', style: Theme.of(context).primaryTextTheme.display1),
                    ),
                  ),
                ),
              ),
            );
            },
        ),
      ),
    );
  }
}