import 'package:AcessAbilities/widgets/hero_with_image_widget.dart';
import 'package:AcessAbilities/widgets/hero_with_text_widget.dart';
import 'package:AcessAbilities/widgets/image_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:AcessAbilities/app/datamodel/discover_card_model.dart';
import 'package:AcessAbilities/app/utils/constants.dart';
import 'package:AcessAbilities/widgets/text_and_button_widget.dart';
import 'package:AcessAbilities/widgets/text_and_image_widget.dart';


class DiscoverPageListView extends StatelessWidget {
  DiscoverPageListView();

  final List<ListItem> listItems = [
    HeroItemWithText(
        'images/expo_cover_photo.png',
        'AccessAbilities Expo',
        "Exhibition for People of Determination",
        "Latest robotic, assistive technology products that enhance the lives of more than 50 million people in the region."),
    HeroItemWithImage(
        "images/sunny_cover_photo.png",
        "images/tigerspike_logo.png",
        "Empowering Women Of Determination",
        "The Sunnny Mobile App empowers women with disabilities to recognise and respond to violence and abuse."),
    ItemWithImage(
        'images/tigerspike.png'),
    TextAndButtonItem("Not Registered?", "Register to visit", Constants.black,
        Constants.white),
    TitleAndImagesItem("Gold Sponsors", "images/gold_sponsors.png"),
    TitleAndImagesItem("Strategic Partners", "images/strategic_sponsors.png")
  ];

  // The base class for the different types of items the list can contain.
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: new AppBar(
          title: new Text("Discover",
              style: TextStyle(
                  fontSize: Constants.headerFontSize,
                  fontFamily: "Avenir",
                  fontWeight: FontWeight.w900,
                  color: Constants.black))),
      body: Container(
        child: ListView.builder(
          itemCount: listItems.length,
          itemBuilder: (BuildContext context, int index) {
            final item = listItems[index];
            if (item is HeroItemWithText) {
              return new HeroWithTextWidget(heroItem: item,
                                            linkUrl: "https://www.accessabilitiesexpo.com/en-gb.html",
                                            linkDisplayText: "Learn More >",
                                            linkTextColour: Constants.aaeOrange);
            } else if (item is TextAndButtonItem) {
              return new TextAndButtonWidget(item: item, url:"https://cloudme02.infosalons.biz/reg/ACCESS19DU/");
            } else if (item is HeroItemWithImage) {
              return new HeroWithImageWidget(heroItem: item,
                                              linkUrl: "https://tigerspike.com/work/1800respect-sunny/",
                                              linkDisplayText: "Learn More >",
                                              linkTextColour: Constants.aaeOrange);
            } else if (item is TitleAndImagesItem) {
              return new TextAndImageWidget(item: item);
            } else if (item is ItemWithImage) {
              return new ImageWidget(heroItem: item,
                  linkUrl: "https://tigerspike.com",
                  linkDisplayText: "Learn More >",
                  linkTextColour: Constants.aaeOrange);
            }
          },
        ),
      ),
    );
  }
}


