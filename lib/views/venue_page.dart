import 'dart:async';
import 'package:AcessAbilities/app/utils/constants.dart';
import 'package:AcessAbilities/widgets/web_view_container.dart';
import 'package:flutter/material.dart';

class VenuePage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: new AppBar(
            title: new Text("Venue",
                style: TextStyle(
                    fontSize: Constants.headerFontSize,
                    fontFamily: "Avenir",
                    fontWeight: FontWeight.w900,
                    color: Constants.black))),
        body: SingleChildScrollView(child: Column(children: <Widget>[
          Padding(
            padding: EdgeInsets.only(left: 10, top: 20, bottom: 20),
            child: SizedBox(
              width: double.infinity,
              child: Container(
                child: Text("Location",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        color: Constants.black)),
              ),
            ),
          ),
          GestureDetector(
              onTap: () {},
              child: Padding(
                  padding: EdgeInsets.only(bottom: 1.0),
                  child: SizedBox(
                      child: Image.asset("images/location_image.png")))),
          Wrap(children: <Widget>[
            Padding(
                padding: EdgeInsets.only(top: 4, bottom: 4, left: 10),
                child: RichText(
                    textAlign: TextAlign.left,
                    text: TextSpan(children: <TextSpan>[
                      TextSpan(
                          text:
                          "Dubai International Convention & Exhibition Centre",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 20,
                              fontWeight: FontWeight.normal)),
                    ])))
          ]),
          Wrap(children: <Widget>[
            SizedBox(
                width: double.infinity,
                child: Padding(
                    padding: EdgeInsets.only(left: 10),
                    child: RichText(
                        textAlign: TextAlign.left,
                        text: TextSpan(children: <TextSpan>[
                          TextSpan(
                              text: "Halls 5, 6 & 7",
                              style: TextStyle(
                                  color: Colors.grey,
                                  fontSize: 15,
                                  fontWeight: FontWeight.normal)),
                        ]))))
          ]),
          Wrap(children: <Widget>[
            SizedBox(
                width: double.infinity,
                child: Padding(
                    padding: EdgeInsets.only(left: 10),
                    child: RichText(
                        textAlign: TextAlign.left,
                        text: TextSpan(children: <TextSpan>[
                          TextSpan(
                              text: "10AM - 6PM",
                              style: TextStyle(
                                  color: Colors.grey,
                                  fontSize: 15,
                                  fontWeight: FontWeight.normal)),
                        ]))))
          ]),
          Padding(
              padding:
              EdgeInsets.only(left: 15, right: 15, top: 8.0, bottom: 16.0),
              child: SizedBox(
                  width: double.infinity, // match_parent

                  child: MaterialButton(
                      padding: const EdgeInsets.only(top: 15.0, bottom: 15),
                      shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(5.0),
                          side: BorderSide(color: Colors.black)),
                      child: Text("Directions",
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.bold)),
                      onPressed: () => openMaps(context),
                      color: Colors.black,
                      textColor: Colors.white,
                      splashColor: Constants.aaeOrange)))
        ])));
  }

  openMaps(BuildContext context) {
    Navigator.push(context, MaterialPageRoute(builder: (context)
    =>
        WebViewContainer("https://goo.gl/maps/TRapFgZUPYYisSww6")));
  }
}
