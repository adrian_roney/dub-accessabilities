import 'package:flutter/material.dart';

class Constants {

  static const Map<int, Color> color =
  {
    50:Color.fromRGBO(136,14,79, .1),
    100:Color.fromRGBO(136,14,79, .2),
    200:Color.fromRGBO(136,14,79, .3),
    300:Color.fromRGBO(136,14,79, .4),
    400:Color.fromRGBO(136,14,79, .5),
    500:Color.fromRGBO(136,14,79, .6),
    600:Color.fromRGBO(136,14,79, .7),
    700:Color.fromRGBO(136,14,79, .8),
    800:Color.fromRGBO(136,14,79, .9),
    900:Color.fromRGBO(136,14,79, 1),
  };

  static const MaterialColor white = MaterialColor(0xFFFFFFFF, color);
  static const MaterialColor aaeOrange = MaterialColor(0xFFE7471B, color);
  static const MaterialColor grey = MaterialColor(0xFF767676, color);
  static const MaterialColor listSeperatoColor = MaterialColor(0xEEEEEEEE, color);
  static const MaterialColor black = MaterialColor(0xFF000000, color);
  static const MaterialColor mapBackground = MaterialColor(0xFFF7F6F5, color);




  static const double headerFontSize = 36;
}