class Exhibitor {
    final String imagefile;
    final String name;
    final String stand;

    Exhibitor({this.imagefile, this.name, this.stand});

    factory Exhibitor.fromJson(Map<String, dynamic> json) {
        return new Exhibitor(
            imagefile: json['imagefile'], 
            name: json['name'],
            stand: json['stand']
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['imagefile'] = this.imagefile;
        data['name'] = this.name;
        data['stand'] = this.stand;
        return data;
    }
}