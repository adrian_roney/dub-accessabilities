import 'package:flutter/material.dart';

abstract class ListItem {}

// A ListItem that contains data to display a heading.
class HeroItemWithText implements ListItem {

  final String heroImagePath;
  final String titleText;
  final String largeTextBanner;
  final String subBanner;

  String get heroImage {
    return heroImagePath;
  }

  String get headerText {
    return titleText;
  }

  String get textBanner {
    return largeTextBanner;
  }

  String get subBannerText {
    return subBanner;
  }

  HeroItemWithText(this.heroImagePath, this.titleText, this.largeTextBanner, this.subBanner);
}
class HeroItemWithImage implements ListItem {

  final String heroImagePath;
  final String headerImagePath;
  final String largeTextBanner;
  final String subBanner;

  String get heroImage {
    return heroImagePath;
  }

  String get headerImage {
    return headerImagePath;
  }

  String get textBanner {
    return largeTextBanner;
  }

  String get subBannerText {
    return subBanner;
  }

  HeroItemWithImage(this.heroImagePath, this.headerImagePath, this.largeTextBanner, this.subBanner);
}

class ItemWithImage implements ListItem {

  final String heroImagePath;
  String get heroImage {
    return heroImagePath;
  }

  ItemWithImage(this.heroImagePath);
}

// A ListItem that contains data to display a message.
class TextAndButtonItem implements ListItem {
  final String headerText;
  final String buttonText;
  final Color buttonColor;
  final Color buttonTextColor;

  String get headerTextString {
    return this.headerText;
  }

  String get buttonTextString {
    return this.buttonText;
  }

  Color get buttonBackgroundColor {
    return this.buttonColor;
  }

  TextAndButtonItem(this.headerText, this.buttonText, this.buttonColor, this.buttonTextColor);

  Color get buttonTextStringColor {
    return this.buttonTextColor;
  }
}

class TitleAndImagesItem implements ListItem {
  final String headerText;
  final String imagePath;

  TitleAndImagesItem(this.headerText, this.imagePath);
}



