import 'package:AcessAbilities/app/datamodel/discover_card_model.dart';
import 'package:AcessAbilities/app/utils/constants.dart';
import 'package:AcessAbilities/widgets/web_view_container.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TextAndButtonWidget extends StatelessWidget {
  final String url;

  const TextAndButtonWidget({
    Key key,
    @required this.item, @required this.url
  }) : super(key: key);

  final TextAndButtonItem item;

  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
      Padding(
          padding: EdgeInsets.only(top: 30.0, bottom: 8.0),
          child: Text(item.headerText,
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontSize: 18,
                  fontFamily: "Avenir Heavy",
                  fontWeight: FontWeight.normal,
                  color: Constants.grey))),
      Padding(
          padding: EdgeInsets.only(left: 15, right: 15, top: 8.0, bottom: 16.0),
          child: SizedBox(
              width: double.infinity, // match_parent

              child: MaterialButton(
                  padding: const EdgeInsets.only(top: 15.0, bottom: 15),
                  shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(5.0),
                      side: BorderSide(color: item.buttonBackgroundColor)),
                  child: Text(item.buttonTextString,
                      style:
                      TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
                  onPressed: () => _registerForEvent(context),
                  color: item.buttonBackgroundColor,
                  textColor: item.buttonTextStringColor,
                  splashColor: Constants.aaeOrange))),
      Container(
        height: 1,
        color: Constants.listSeperatoColor,
      ),
    ]);
  }

  void _registerForEvent(BuildContext context) {
    Navigator.push(context, MaterialPageRoute(builder: (context) => WebViewContainer(this.url)));
  }
}