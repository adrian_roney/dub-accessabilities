import 'package:flutter/cupertino.dart';

import 'package:AcessAbilities/app/datamodel/discover_card_model.dart';
import 'package:AcessAbilities/app/utils/constants.dart';

class TextAndImageWidget extends StatelessWidget {
  const TextAndImageWidget({
    Key key,
    @required this.item,
  }) : super(key: key);

  final TitleAndImagesItem item;

  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
      Padding(
          padding: EdgeInsets.only(left: 35, right: 35, top: 8.0, bottom: 16.0),
          child: Text(item.headerText,
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.normal,
                  color: Constants.grey))),
      Padding(
          padding: EdgeInsets.only(bottom: 16.0),
          child: SizedBox(child: Image.asset(item.imagePath))),
      Container(
        height: 1,
        color: Constants.listSeperatoColor,
      ),
    ]);
  }
}