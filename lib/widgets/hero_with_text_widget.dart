import 'package:flutter/material.dart';

import 'package:AcessAbilities/app/utils/constants.dart';
import 'package:AcessAbilities/app/datamodel/discover_card_model.dart';

import 'link_to_url_widget.dart';


class HeroWithTextWidget extends StatelessWidget {

  final String linkUrl;
  final String linkDisplayText;
  final Color linkTextColour;

  const HeroWithTextWidget({
    Key key,
    @required this.heroItem, this.linkUrl, this.linkDisplayText, this.linkTextColour,
  }) : super(key: key);

  final HeroItemWithText heroItem;

  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
      Padding(
          padding: EdgeInsets.only(left:40, right: 40, top:40, bottom: 8.0),
          child: SizedBox(child: Image.asset(heroItem.heroImage))),
      Padding(
          padding: EdgeInsets.only(top: 16.0, bottom: 8.0),
          child: Text(heroItem.titleText,
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold))),
      Padding(
          padding: EdgeInsets.only(left: 15, right: 15, top: 8.0, bottom: 16.0),
          child: Text(heroItem.textBanner,
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 36, fontWeight: FontWeight.bold))),
      Padding(
          padding: EdgeInsets.only(left: 15, right: 15, top: 8.0, bottom: 16.0),
          child: Text(heroItem.subBannerText,
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.normal,
                  color: Constants.grey))),
      Padding(
        padding: EdgeInsets.only(left: 35, right: 35, top: 8.0, bottom: 16.0),
        child: LinkToUrlWidget(url: this.linkUrl,
            linkDisplayText: this.linkDisplayText,
            textColor: this.linkTextColour)),
      Container(
        height: 1,
        color: Constants.listSeperatoColor,
      ),
    ]);
  }
}
