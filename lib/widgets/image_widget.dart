import 'package:flutter/cupertino.dart';

import 'package:AcessAbilities/app/datamodel/discover_card_model.dart';
import 'package:AcessAbilities/app/utils/constants.dart';

import 'link_to_url_widget.dart';


class ImageWidget extends StatelessWidget {
  final String linkUrl;
  final String linkDisplayText;
  final Color linkTextColour;


  const ImageWidget({
    Key key,
    @required this.heroItem, this.linkUrl, this.linkDisplayText, this.linkTextColour,
  }) : super(key: key);

  final ItemWithImage heroItem;

  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
      Padding(
          padding: EdgeInsets.only(top:25, left:100, right:100, bottom: 15),
          child: SizedBox(child: Image.asset(heroItem.heroImage))),
      Padding(
          padding: EdgeInsets.only(left: 35, right: 35, top: 8.0, bottom: 16.0),
          child: LinkToUrlWidget(url: this.linkUrl,
                                 linkDisplayText: this.linkDisplayText,
                                 textColor: this.linkTextColour),
      ),


      Container(
        height: 1,
        color: Constants.listSeperatoColor,
      ),
    ]);
  }
}