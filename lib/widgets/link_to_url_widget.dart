import 'package:AcessAbilities/app/datamodel/discover_card_model.dart';
import 'package:AcessAbilities/app/utils/constants.dart';
import 'package:AcessAbilities/widgets/web_view_container.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LinkToUrlWidget extends StatelessWidget {



  const LinkToUrlWidget({
    Key key,
    @required String this.url,
    @required String this.linkDisplayText,
    @required Color this.textColor
  }
  ) : super(key: key);

  final String url;
  final String linkDisplayText;
  final Color textColor;

  @override
  Widget build(BuildContext context) {
    return
      GestureDetector(
        onTap: () {
          Navigator.push(context, MaterialPageRoute(builder: (context) => WebViewContainer(this.url)));
        },
            child: Text(linkDisplayText,
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 18,
                    fontFamily: "Avenir Heavy",
                    fontWeight: FontWeight.normal,
                    color: this.textColor)),

    );
  }

  void _registerForEvent() {}
}
